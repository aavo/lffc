/* ******* */
/* Palette */
/* ******* */
@land:              #ffffff;
@residential:       #c1bfba;
@water:             #c5d2dd;
@neutral:           #e2e2e6;
@wooded:            #c5ddd0;

@road_fill:         #ddceb3;
@road_case:         #574627;
@road:              #9d9d9d;

@rail_fill:         #ddd;
@rail_case:         #777;

@city_text:         #222;
@city_halo:         @land;
@town_text:         #222;
@town_halo:         @land;
@village_text:      #222;
@village_halo:      @land;

@road_text:         #222;
@road_halo:         @land;

@runway:            #666;
@runway_fill:       #ccc;
@contour_line:      #d2ccb0;
@tma:               #c17474;
@tmablue:           #6588a5;
@transit:           #4c6a83;
@local:             #b85257;
@zone:              #c0392b;
@zone55:            #e9d460;
@zone65:            #3fc380;
@zonebeynes:        #777;

@regular:           'Fira Sans Regular';
@bold:              'Fira Sans Bold';
@medium:            'Fira Sans Medium';
@book:              'Fira Sans Book';
@ibook:             'Fira Sans Book Italic';
@xlight:            'Fira Sans ExtraLight';

@buffer:            512;

/* *********** */
/* backgrounds */
/* *********** */

Map {
  // background-color: @water;
  buffer-size: @buffer;
}
#land {
  polygon-fill: @land;
}
#landuse_gen[zoom<11],
#landuse[type="residential"][zoom>=12],
#landuse[zoom>=11] {
  #landuse_gen {
      polygon-opacity: 0.8;
  }
  #landuse_gen[zoom>=12] {
      polygon-opacity: 0.9;
  }
  [type='grave_yard'],
  [type='college'],
  [type='school'],
  [type='education'],
  [type='sports_centre'],
  [type='stadium'],
  [type='university'],
  [type='cemetery'],
  [type='hospital'],
  [type='industrial'],
  [type='landfill'],
  [type='quarry'],
  [type='commercial'],
  [type='residential'],
  [type='retail'],
  [type='playground'],
  [type='pedestrian'] {
      polygon-fill: @residential;
  }
  [type='golf_course'],
  [type='pitch'],
  [type='grass'],
  [type='grassland'],
  [type='park'],
  [type='garden'],
  [type='village_green'],
  [type='recreation_ground'],
  [type='picnic_site'],
  [type='camp_site'],
  [type='common'],
  [type='scrub'],
  [type='meadow'],
  [type='heath'],
  [type='wetland'],
  [type='farmland'],
  [type='farm'],
  [type='orchard'],
  [type='allotments'] {
      polygon-fill: @land;
  }
  [type='basin'] {
      polygon-fill: @water;
  }
  [type='forest'],
  [type='wood'] {
      polygon-fill: @wooded;
  }
}

#waterareas {
  polygon-fill: @water;
}


#waterway[type='river'] {
  line-color: @water;
  line-width: 4;
  line-cap: round;
  line-join: round;
}

/* ******** */
/* RAILWAYS */
/* ******** */
#railway[zoom>=11] {
  outline/line-width: 3;
  outline/line-color: @rail_case;
  outline/line-cap: round;
  line-color: @rail_fill;
  line-width: 2;
  line-dasharray: 5,5;
  [zoom>=15] {
    outline/line-width: 2;
  line-width: 2;
  }
  [zoom>=17] {
    outline/line-width: 3;
  }
}


/* ******** */
/*  ROADS   */
/* ******** */
#roads::casing[type='primary'],
#roads::casing[type='primary_link'],
#roads::casing[type='trunk'],
#roads::casing[type='trunk_link'],
#roads::casing[type='motorway_link'],
#roads::casing[type='motorway'] {
  line-color: @road_case;
  line-width: 3;
  line-join: round;
  [type='motorway'] {
      line-width: 5;
  }
}
#roads[type='primary'],
#roads[type='primary_link'],
#roads[type='trunk'],
#roads[type='trunk_link'],
#roads[type='motorway_link'],
#roads[type='motorway'] {
  line-color: @road_fill;
  line-join: round;
  line-cap: square;
  line-width: 2;
  [type='motorway'] {
      line-width: 4;
  }
}
#roads[type='tertiary'],
#roads[type='secondary'] {
  line-color: lighten(@road, 10%);
  line-width: 1;
}
#bridge {
  marker-file: url('icons/bridge.svg');
  [lock=true] {
    marker-file: url('icons/lock.svg');
  }
  marker-clip: false;
  marker-transform: 'rotate([orientation])';
  marker-ignore-placement: true;
  marker-width: 40;
  [zoom<=11] {
    marker-width: 30;
  }
}


/* ****** */
/* BUILDINGS */
/* ****** */
#buildings[zoom>=14] {
  polygon-fill: #aaa;
  [zoom>=15] {
      line-color: #ddd;
      [type!="yes"] {
        polygon-fill: #ddd;
        line-color: #ccc;
      }
  }
}

/* ================================================================== */
/* RELIEF
/* ================================================================== */

#hillshade {
    raster-scaling: bilinear;
    raster-comp-op: multiply;
    raster-opacity: 0.85;
}
