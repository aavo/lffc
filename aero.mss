#aeroway_line[type="runway"] {
    line-color: @runway;
    line-width: 5;
    line-cap: square;
}

#local[zoom<12][label='10 km'],
#local[zoom<12][label='20 km'],
#local[zoom<12][label='30 km'],
#local[zoom=12][code='lffc'] {
    line-color: @local;
    line-dasharray: 20,10;
    line-width: 2.5;
}

#tma::internal {
    line-color: @tma;
    [color='blue'] {
        line-color: @tmablue;
    }
    line-offset: -4;
    line-width: 8;
    line-opacity: 0.5;
}

#tma {
    line-color: @tma;
    line-width: 2;
    [color='blue'] {
        line-color: @tmablue;
    }
}

#transit[zoom<12] {
    line-color: @transit;
    line-width: 5;
    line-dasharray: 1,15;
    line-cap: round;
}
#transit_label[zoom<12] {
    text-name: '[label]';
    text-face-name: @book;
    text-placement: line;
    text-spacing: 200;
    text-fill: @transit;
    text-size: 14;
    text-dy: 6;
    text-halo-fill: @village_halo;
    text-halo-radius: 2;
    text-character-spacing: 0.1;
}

#transit_point[ref!=null][zoom<12] {
  marker-file: url('icons/transit.svg');
  marker-clip: false;
  marker-ignore-placement: true;
  marker-height: 32;
}
#transit_point::label[ref!=null][zoom<12] {
  text-name: '[ref]';
  text-face-name: @bold;
  text-placement: point;
  text-fill: @transit;
  text-size: 16;
  text-halo-fill: @village_halo;
  text-halo-radius: 2;
  text-wrap-width: 30;
  text-label-position-tolerance: 20;
  text-character-spacing: 0.1;
  text-dx: -15;
}

#zone[zoom<12] {
    polygon-fill: @zone;
    polygon-opacity: 0.3;
    [AH='FL055'] {
        polygon-fill: @zone55;
    }
    [AH='FL065'] {
        polygon-fill: @zone65;
    }
}

#beynes::internal[zoom<12] {
    line-color: @zonebeynes;
    line-offset: -4;
    line-width: 8;
    line-opacity: 0.3;
}

#beynes[zoom<12] {
    line-color: @zonebeynes;
    line-width: 2;
}
