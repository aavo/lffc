# LFFC - map for Chérence airport


## Import

    env PGHOST=/var/run/postgresql/ imposm3 import -mapping mapping.yml -read ~/Data/geo/pbf/ile-de-france-latest.osm.pbf -connection="postgis:///lffc" -write -overwritecache
    env PGHOST=/var/run/postgresql/ imposm3 import -mapping mapping.yml -read ~/Data/geo/pbf/picardie-latest.osm.pbf -connection="postgis:///lffc" -write -appendcache
    env PGHOST=/var/run/postgresql/ imposm3 import -mapping mapping.yml -read ~/Data/geo/pbf/haute-normandie-latest.osm.pbf -connection="postgis:///lffc" -write -appendcache -deployproduction


## DEM

Data from http://dwtkns.com/srtm30m/

    gdalwarp -s_srs EPSG:4326 -t_srs EPSG:3785 -r bilinear data/N48E001.hgt data/N48E001-3785.tif
    gdaldem hillshade -co compress=lzw -z 3 -alt 75 data/N48E001-3785.tif data/N48E001-3785-hillshade.tif
    gdalbuildvrt data/hillshade.vrt (pwd)/data/N49E001-3785-hillshade.tif


## Export

    kosmtik export project.yml --format png8 --output tmp/export.png --width 2500 --height 3500 --scale 2
