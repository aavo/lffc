exportlocal:
	node ~/Code/js/kosmtik/index.js export project.yml --format png8 --output tmp/local.png --width 2266 --height 3399 --scale 2 --bounds 1.47048,48.89612,1.84126,49.26031
exportmanw:
	node ~/Code/js/kosmtik/index.js export project.yml --format png8 --output tmp/manw.png --width 1860 --height 2790 --scale 2 --bounds 1.23046,48.70546,1.92810,49.40114
export: exportlocal exportmanw
download:
	rm tmp/pbf/* || true
	aria2c http://download.geofabrik.de/europe/france/centre-latest.osm.pbf --dir tmp/pbf
	aria2c http://download.geofabrik.de/europe/france/haute-normandie-latest.osm.pbf --dir tmp/pbf
	aria2c http://download.geofabrik.de/europe/france/ile-de-france-latest.osm.pbf --dir tmp/pbf
	aria2c http://download.geofabrik.de/europe/france/picardie-latest.osm.pbf --dir tmp/pbf
import:
	env PGHOST=/var/run/postgresql/ imposm3 import -mapping mapping.yml -read tmp/pbf/ile-de-france-latest.osm.pbf -connection="postgis:///lffc" -write -overwritecache
	env PGHOST=/var/run/postgresql/ imposm3 import -mapping mapping.yml -read tmp/pbf/picardie-latest.osm.pbf -connection="postgis:///lffc" -write -appendcache
	env PGHOST=/var/run/postgresql/ imposm3 import -mapping mapping.yml -read tmp/pbf/centre-latest.osm.pbf -connection="postgis:///lffc" -write -appendcache
	env PGHOST=/var/run/postgresql/ imposm3 import -mapping mapping.yml -read tmp/pbf/haute-normandie-latest.osm.pbf -connection="postgis:///lffc" -write -appendcache -deployproduction
pdf:
	weasyprint templates/local.html tmp/local_`date +"%Y%m%d"`.pdf
