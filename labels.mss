/* ************************* */
/*          POI            */
/* ************************* */
#poi[icon!=''] {
  marker-file: url('icons/[icon].svg');
  marker-clip: false;
  marker-height: 64;
  [icon='legaz'] {
    marker-height: 32;
  }
}
#poi::label[label!=null][zoom=12],
#poi::label[label!=null][zoom<=11][campagne=1] {
  text-name: '[label]';
  text-face-name: @ibook;
  text-placement: point;
  text-fill: lighten(@village_text, 20);
  text-size: 16;
  [importance=1] {
    text-fill: @village_text;
  }
  text-halo-fill: @village_halo;
  text-halo-radius: 2;
  text-wrap-width: 30;
  text-label-position-tolerance: 20;
  text-character-spacing: 0.1;
  [label='Le Gaz'] {
    text-dy: 20;
  }
  [zoom<=11][label='Les Noyers'] {
    text-dy: -5;
  }
}


/* ************************* */
/*          PLACE            */
/* ************************* */
#place[zoom=12],
#place[zoom<12][importance>=1] {
  text-name: '[name].replace("Saint-", "St-").replace("Sainte-", "Ste-").replace("-sur-", "/")';
  [zoom<=11][vhf!=''] {
    text-name: '[name] + "\n(" + [vhf] + ")"';
  }
  text-face-name: @book;
  text-placement:point;
  text-fill: @village_text;
  text-size: 15;
  text-halo-fill: @village_halo;
  text-halo-radius: 2;
  text-wrap-width: 30;
  text-label-position-tolerance: 20;
  text-character-spacing: 0.1;
  text-wrap-width: 30;
  [zoom>=12] {
    text-wrap-character: '-';
    text-repeat-wrap-character: true;  // mapnik 3.x only
  }
  text-line-spacing: -2;
  [zoom<=11][name="Maudétour"] {
    text-dx: 10;
  }
  [zoom<=11][name="Gasny"] {
    text-dx: -5;
  }
  [zoom<=11][name="Serans"] {
    text-dx: -18;
    text-dy: -10;
  }
  [zoom<=11][name="Porcheville"] {
    text-dx: 1;
    text-dy: -1;
  }
  [zoom<=11][name="Mantes"] {
    text-dy: -5;
  }
  [zoom<=11][name="Vernon"] {
    text-dy: 3;
  }
  [importance=2] {
    text-size: 18;
    text-fill: @town_text;
    text-halo-fill: @town_halo;
    text-face-name: @medium;
  }
  [importance=1] {
    text-size: 18;
    text-fill: @city_text;
    text-halo-fill: @city_halo;
    text-face-name: @medium;
    text-transform: uppercase;
  }
}


/* ************************* */
/*          ROADS            */
/* ************************* */
#road_label::shield[type='motorway'],
#road_label::shield[type='trunk'][zoom>=9] {
  shield-name: "[ref].replace('·', '\n')";
  shield-size: 12;
  shield-line-spacing: -4;
  shield-file: url('icons/shield.svg');
  shield-face-name: @book;
  shield-fill: #333;
  shield-spacing: 100;
  shield-repeat-distance: 100;
  shield-min-padding: 1;
  shield-margin: 10;
}


/* ************************* */
/*          RIVER            */
/* ************************* */
#waterway_label[type='river'] {
  text-name: '[name]';
  text-face-name: @book;
  text-fill: darken(@water, 40%);
  text-halo-fill: @neutral;
  text-halo-radius: 1;
  text-placement: line;
  text-min-distance: 200;
  text-size: 14;
  text-dy: 7;
}


/* ************************* */
/*        AEROWAY            */
/* ************************* */
#threshold {
  text-name: '[name]';
  text-face-name: @book;
  text-fill: darken(@runway, 40%);
  text-halo-fill: @neutral;
  text-allow-overlap: true;
  text-size: 14;
  text-orientation: '360 - [orientation]';
  text-dx: 1;
  text-dy: -5;
  [name='04'] {
    text-dx: -1;
    text-dy: 3;
  }
  [name='30'] {
    text-dx: 2;
    text-dy: 2;
  }
  [name='12'] {
    text-dx: -2;
    text-dy: -2;
  }
}
#local_label[zoom<=11][label='10 km'],
#local_label[zoom<=11][label='20 km'],
#local_label[zoom<=11][label='30 km'],
#local_label[zoom=12][code='lffc'] {
  text-name:'[label]';
  text-face-name: @book;
  text-placement: line;
  text-spacing: 200;
  text-fill: darken(@local, 10%);
  text-size: 17;
  text-halo-fill: @village_halo;
  text-halo-radius: 2;
  text-character-spacing: 0.1;
  text-margin: 20;
}

#tma_label {
    text-name:'[name]';
    [zoom>=12][upper!=null] {
      text-name:'[name] + " — " + [upper]';
    }
    text-face-name: @book;
    text-placement: line;
    text-spacing: 200;
    text-fill: darken(@tma, 20);
    text-margin: 10;
    [color='blue'] {
        text-fill: darken(@tmablue, 20);
    }
    text-size: 14;
    text-dy: -10;
    text-halo-fill: @village_halo;
    text-halo-radius: 2;
    text-character-spacing: 0.1;
}

#beynes_label[zoom<12] {
    text-name:'[name]';
    text-face-name: @book;
    text-placement: line;
    text-spacing: 200;
    text-fill: darken(@zonebeynes, 20);
    text-margin: 10;
    text-size: 12;
    text-dy: -10;
    text-halo-fill: @village_halo;
    text-halo-radius: 2;
    text-character-spacing: 0.1;
}

#zone_label[zoom<12] {
    text-name:'[AH]';
    text-face-name: @ibook;
    text-placement: line;
    text-spacing: 200;
    text-fill: darken(@zone, 30);
    text-margin: 10;
    text-size: 12;
    text-dy: -6;
    text-halo-fill: @village_halo;
    text-halo-radius: 2;
    text-character-spacing: 0.1;
    [AH='FL055'] {
        text-fill: darken(@zone55, 40);
    }
    [AH='FL065'] {
        text-fill: darken(@zone65, 30);
    }
}

#flight_level[zoom>=12] {
    shield-name: "[upper] + 'M QFE'";
    shield-size: 16;
    shield-fill: darken(@local, 25);
    shield-line-spacing: -4;
    shield-file: url('icons/flight_level.svg');
    shield-face-name: @book;
    shield-spacing: 100;
    shield-repeat-distance: 100;
    shield-min-padding: 1;
}
